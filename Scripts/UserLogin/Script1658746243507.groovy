import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://shop.demoqa.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/UserLogin/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))

WebUI.click(findTestObject('Object Repository/UserLogin/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))

WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/h2_Login'))

WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/label_Username or email address'))

WebUI.setText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__username'), 'userdummy9291')

WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/label_Password'))

'SuperDummy678'
WebUI.setEncryptedText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__password'), 
    'EpjaIrbb+p10LdZ2si7Guw==')

WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/span__show-password-input'))

WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__rememberme'))

WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/span_Remember me'))

WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/button_Log in'))

WebUI.scrollToElement(findTestObject('UserLogin/Page_My Account  ToolsQA Demo Site/a_Log out'), 0)

WebUI.scrollToElement(findTestObject('UserLogin/Page_My Account  ToolsQA Demo Site/a_address'), 0)

WebUI.scrollToElement(findTestObject('UserLogin/Page_My Account  ToolsQA Demo Site/a_dashboard'), 0)

WebUI.verifyTextPresent('userdummy9291', false)

WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/p_From your account dashboard you can view _1b3d94'))

WebUI.scrollToElement(findTestObject('UserLogin/Page_My Account  ToolsQA Demo Site/p_From your account dashboard you can view _1b3d94'), 
    0)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/a_Log out'))

