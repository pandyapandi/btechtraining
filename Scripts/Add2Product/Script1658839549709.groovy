import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://shop.demoqa.com/')

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))

WebUI.setText(findTestObject('Object Repository/Add2Cart/Page_My Account  ToolsQA Demo Site/input__username'), GlobalVariable.username)

WebUI.setEncryptedText(findTestObject('Object Repository/Add2Cart/Page_My Account  ToolsQA Demo Site/input__password'), 
    GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_My Account  ToolsQA Demo Site/button_Log in'))

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_My Account  ToolsQA Demo Site/img_Checkout_custom-logo'))

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_pink drop shoulder oversized t shirt'))

BajuBeli = WebUI.getText(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/h1_pink drop shoulder oversized t shirt'))

System.out.println(BajuBeli)

BajuHarga = WebUI.getText(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/bdi_15.00'))

System.out.println(BajuHarga)

WebUI.scrollToElement(findTestObject('Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/button_Add to cart'), 
    0)

WebUI.selectOptionByValue(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/select_Choose an optionPink'), 
    'pink', true)

WebUI.delay(5)

WebUI.selectOptionByValue(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/select_Choose an option363738'), 
    '38', true)

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/i_QTY_icon_plus'))

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/button_Add to cart'))

BajuSukses = WebUI.getText(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/div_View cart 2  pink drop shoulder oversiz_8b5cdf'))

System.out.println(BajuSukses)

WebUI.click(findTestObject('Object Repository/Add2Cart/Page_pink drop shoulder oversized t shirt  _e751c6/i_Checkout_icon_bag_alt'))

WebUI.scrollToElement(findTestObject('Add2Cart/Page_Cart  ToolsQA Demo Site/button_applycoupon'), 0)

BajuCart = WebUI.getText(findTestObject('Object Repository/Add2Cart/Page_Cart  ToolsQA Demo Site/a_pink drop shoulder oversized t shirt - Pink'))

System.out.println(BajuCart)

BajuJum = WebUI.getText(findTestObject('Object Repository/Add2Cart/Page_Cart  ToolsQA Demo Site/input_QTY_cartf9314662473b1ba0fab377707c680e17qty'))

System.out.println(BajuJum)

WebUI.scrollToElement(findTestObject('Add2Cart/Page_Cart  ToolsQA Demo Site/a_pink drop shoulder oversized t shirt - Pink'), 
    0)

