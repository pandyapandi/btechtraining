import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/*
WebUI.openBrowser('')

WebUI.navigateToUrl('https://shop.demoqa.com/')

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_My Account  ToolsQA Demo Site/input__username'), 'userdummy9291')

WebUI.setEncryptedText(findTestObject('Object Repository/CheckoutCart/Page_My Account  ToolsQA Demo Site/input__password'), 
    'EpjaIrbb+p10LdZ2si7Guw==')

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_My Account  ToolsQA Demo Site/button_Log in'))

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_My Account  ToolsQA Demo Site/i_Checkout_icon_bag_alt'))
**/
WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_Cart  ToolsQA Demo Site/a_Proceed to checkout'))

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_first_name'), 
    'User')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_last_name'), 
    'Dummy9291')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_(optional)_billing_company'), 
    'PT. Dummy')

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/span_Oman'))

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_We Work All The Holidays_select2-sear_c9686a'), 
    'oman')

WebUI.sendKeys(findTestObject('CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_We Work All The Holidays_select2-sear_c9686a'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_(optional)_billing_company'), 
    'PT. Dummy')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_address_1'), 
    'Jalan')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_(optional)_billing_address_2'), 
    'Damai')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_city'), 'Oman 2')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_state'), 'GG')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_postcode'), 
    '777')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_phone'), '936783746')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input__billing_email'), 'userdummy9291@dummy.com')

WebUI.setText(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/textarea_(optional)_order_comments'), 
    'additional')

WebUI.scrollToElement(findTestObject('CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_(optional)_billing_company'), 
    0)

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/input_privacy policy_terms'))

WebUI.click(findTestObject('Object Repository/CheckoutCart/Page_Checkout  ToolsQA Demo Site/button_Place order'))

WebUI.scrollToElement(findTestObject('CheckoutCart/Page_Checkout  ToolsQA Demo Site/p_Thank you. Your order has been received'), 
    0)

WebUI.takeScreenshot()

