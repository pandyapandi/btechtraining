import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

int ranNum

ranNum = ((Math.random() * 999) as int)

WebUI.openBrowser('')

WebUI.navigateToUrl('https://shop.demoqa.com/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('UserRegister/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))

WebUI.click(findTestObject('UserRegister/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))

WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Username'))

WebUI.setText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__username_1'), ranNum + 'userdummy')

WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Password'))

WebUI.setEncryptedText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__password'), 'lwskGQl43AYeBsDVzt1FsQ==')

WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Email address'))

WebUI.setText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__email'), ('userdummy' + ranNum) + '@dummy.com')


WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/p_Your personal data will be used to suppor_aca836'))

WebUI.click(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/span_Medium_show-password-input'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/button_Register'))

