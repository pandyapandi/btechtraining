#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Login User

  @Valid
  Scenario Outline: Login Valid
    Given User navigate to website ShopDemoQA for login
    When User click button my account button for login
    And User input <username> and <password>
    And User click button login
    Then User click button logout

    Examples: 
      | username  | password |
      | userdummy9291 | EpjaIrbb+p10LdZ2si7Guw== |
      
      
      
      
      