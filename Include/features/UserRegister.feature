#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Register User

  @Valid
  Scenario Outline: Register User Valid
    Given User navigate to website ShopDemoQA for register
    When User click button my account button for register
    And User input form register <sandi>
    And User click button register

    Examples: 
      | sandi  |
      | EpjaIrbb+p10LdZ2si7Guw== |