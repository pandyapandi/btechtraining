package com.register.user
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User navigate to website ShopDemoQA for register")
	def User_navigate_to_website_ShopDemoQA_for_register() {
		WebUI.openBrowser('')

		WebUI.navigateToUrl('https://shop.demoqa.com/')

		WebUI.maximizeWindow()

		WebUI.click(findTestObject('UserRegister/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))
	}

	@When("User click button my account button for register")
	def User_click_button_my_account_button_for_register() {
		WebUI.click(findTestObject('UserRegister/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))
	}

	@And("User input form register (.*)")
	def User_input_form_register(String sandi) {

		int ranNum

		ranNum = ((Math.random() * 999) as int)

		WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Username'))

		WebUI.setText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__username_1'), ranNum + 'userdummy')

		WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Password'))

		WebUI.setEncryptedText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__password'), sandi)

		WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/label_Email address'))

		WebUI.setText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/input__email'), ('userdummy' + ranNum) + '@dummy.com')


		WebUI.getText(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/p_Your personal data will be used to suppor_aca836'))

		WebUI.click(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/span_Medium_show-password-input'))
	}

	@And("User click button register")
	def User_click_button_register() {
		WebUI.click(findTestObject('UserRegister/Page_My Account  ToolsQA Demo Site/button_Register'))
	}
}