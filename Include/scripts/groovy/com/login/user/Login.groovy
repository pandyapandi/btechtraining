package com.login.user
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User navigate to website ShopDemoQA for login")
	def User_navigate_to_website_ShopDemoQA_for_login() {
		WebUI.openBrowser('')

		WebUI.navigateToUrl('https://shop.demoqa.com/')

		WebUI.maximizeWindow()

		WebUI.click(findTestObject('Object Repository/UserLogin/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_Dismiss'))
	}

	@When("User click button my account button for login")
	def User_click_button_my_account_button_for_login() {
		WebUI.click(findTestObject('Object Repository/UserLogin/Page_ToolsQA Demo Site  ToolsQA  Demo E-Com_8f3fb4/a_My Account'))
	}

	@And("User input (.*) and (.*)")
	def User_input_username_and_password(String username, String password) {
		WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/h2_Login'))

		WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/label_Username or email address'))

		WebUI.setText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__username'), username)

		WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/label_Password'))

		'SuperDummy678'
		WebUI.setEncryptedText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__password'),
				password)

		WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/span__show-password-input'))

		WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/input__rememberme'))

		WebUI.getText(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/span_Remember me'))
	}

	@And("User click button login")
	def User_click_button_login() {
		WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/button_Log in'))
	}

	@Then("User click button logout")
	def User_click_button_logout() {

		//WebUI.scrollToElement(findTestObject('UserLogin/Page_My Account  ToolsQA Demo Site/a_Log out'), 0)

		WebUI.click(findTestObject('Object Repository/UserLogin/Page_My Account  ToolsQA Demo Site/a_Log out'))
	}
}