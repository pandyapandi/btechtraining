$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/Users/ddbqa/git/22072022/btechtraining/Include/features/UserRegister.feature");
formatter.feature({
  "name": "Register User",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Register User Valid",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "User navigate to website ShopDemoQA for register",
  "keyword": "Given "
});
formatter.step({
  "name": "User click button my account button for register",
  "keyword": "When "
});
formatter.step({
  "name": "User input form register \u003csandi\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click button register",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "sandi"
      ]
    },
    {
      "cells": [
        "EpjaIrbb+p10LdZ2si7Guw\u003d\u003d"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Register User Valid",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "User navigate to website ShopDemoQA for register",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.User_navigate_to_website_ShopDemoQA_for_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button my account button for register",
  "keyword": "When "
});
formatter.match({
  "location": "Register.User_click_button_my_account_button_for_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input form register EpjaIrbb+p10LdZ2si7Guw\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "Register.User_input_form_register(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.User_click_button_register()"
});
formatter.result({
  "status": "passed"
});
});