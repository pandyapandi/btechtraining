$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/Users/ddbqa/git/22072022/btechtraining/Include/features/UserLogin.feature");
formatter.feature({
  "name": "Login User",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Login Valid",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "User navigate to website ShopDemoQA for login",
  "keyword": "Given "
});
formatter.step({
  "name": "User click button my account button for login",
  "keyword": "When "
});
formatter.step({
  "name": "User input \u003cusername\u003e and \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "User click button login",
  "keyword": "And "
});
formatter.step({
  "name": "User click button logout",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "userdummy9291",
        "EpjaIrbb+p10LdZ2si7Guw\u003d\u003d"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Login Valid",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@Valid"
    }
  ]
});
formatter.step({
  "name": "User navigate to website ShopDemoQA for login",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.User_navigate_to_website_ShopDemoQA_for_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button my account button for login",
  "keyword": "When "
});
formatter.match({
  "location": "Login.User_click_button_my_account_button_for_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input userdummy9291 and EpjaIrbb+p10LdZ2si7Guw\u003d\u003d",
  "keyword": "And "
});
formatter.match({
  "location": "Login.User_input_username_and_password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button login",
  "keyword": "And "
});
formatter.match({
  "location": "Login.User_click_button_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button logout",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.User_click_button_logout()"
});
formatter.result({
  "status": "passed"
});
});